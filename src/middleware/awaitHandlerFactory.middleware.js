const awaitHandlerFactory = (middleware) => {
    return async (req, res, next) => {
        try {
            await middleware(req, res, next)
        } catch (err) {
            console.log('await middleware catch error');
            next(err)
        }
    }
}

module.exports = awaitHandlerFactory;