const helpers = require('../utils/imageUpload.utils');
const UserModel = require('../models/user.model');
const HttpException = require('../utils/HttpException.utils');
const {getResponceObject, getCurrentUserByToken} =  require('../utils/common.utils');
const multer = require('multer');
const path = require('path');
const { updateProfilePicUrl } = require('../models/user.model');
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '_' + Date.now() + path.extname(file.originalname));
    }
});
function upload(req, res, next){
    // 'profile_pic' is the name of our file input field in the HTML form
    
    let uploads = multer({ storage: storage,limits: {fileSize: 100000000}, fileFilter: helpers.imageFilter }).single('profile_pic');

    uploads(req, res, function(err) {
        // req.file contains information of uploaded file
        // req.body contains information of text fields, if there were any
        let isError = false;
        if (req.fileValidationError) {
            isError = true;
            return res.status(400).json(getResponceObject('error','$400', req.fileValidationError));
        }
        else if (!err && !req.file) {
            isError = true;
            return res.status(400).json(getResponceObject('error','$400', 'Please select an image to upload'));
        }
        else if (err instanceof multer.MulterError) {
            isError = true;
            return res.status(400).json(getResponceObject('error','$400', err.message || 'Error while uploading'));
        }
        else if (err) {
            isError = true;
            return res.status(400).json(getResponceObject('error','$400', err));
        }

        // Display uploaded image for user validation
        let uploadStatus = getResponceObject('success','$201', 'File uploaded successfully.');
        uploadStatus['filePath'] = `/api/v1/image/${req.file.path}`;
        
        if(!isError) {
            const currentUserID = getCurrentUserByToken(req.headers.authtoken);
            const query = updateProfilePicUrlInDB(currentUserID.username, {profilePicUrl: uploadStatus['filePath']})
            if(query) {
                res.status(200).json(uploadStatus);
            } else {
                res.status(400).json(getResponceObject('error','$500', 'Error while updating profile picture.'));
            }
        }
    });
}

module.exports = {
    upload: upload
}


async function updateProfilePicUrlInDB(currentUserID, filePath){
    try {
        const update = await UserModel.updateProfilePicUrl(currentUserID, filePath);
        return update;
    } catch (error) {
        console.log(error);
        throw new HttpException(500, 'Internal server error');
    }
}
