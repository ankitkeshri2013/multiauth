const UserModel = require('../models/user.model');
const HttpException = require('../utils/HttpException.utils');
const { validationResult } = require('express-validator');
const { getResponceObject } = require('../utils/common.utils');
const mailEngine = require('../utils/mailer');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              User Controller
 ******************************************************************************/
class UserController {
      async getAllUsers(req, res, next) {
        let userList = await UserModel.find();
        if (!userList.length) {
            throw new HttpException(404, 'Users not found');
        }

        userList = userList.map(user => {
            const { password, ...userWithoutPassword } = user;
            return userWithoutPassword;
        });

        res.send(userList);
    }

    async getUserById (req, res, next) {
        const user = await UserModel.findOne({ id: req.params.id });
        if (!user) {
            throw new HttpException(404, 'User not found');
        }

        const { password, ...userWithoutPassword } = user;

        res.send(userWithoutPassword);
    }

    async getUserByuserName(req, res, next) {
        console.log('finding user for ', req);
        const user = await UserModel.findOne({ username: req.params.username });
        if (!user) {
            throw new HttpException(404, 'User not found');
        }

        const { password, ...userWithoutPassword } = user;

        res.send(userWithoutPassword);
    }

    async getCurrentUser (req, res, next) {
        const { password, ...userWithoutPassword } = req.currentUser;

        res.send(userWithoutPassword);
    }

    async createUser(req, res, next) {
        checkValidation(req);

        await hashPassword(req);

        const result = await UserModel.create(req.body);
        console.log('result', result);
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).json(getResponceObject('success', '$201', 'User created.'));
    }

    async updateUser(req, res, next) {
        checkValidation(req);

        await hashPassword(req);

        const { confirm_password, ...restOfUpdates } = req.body;

        // do the update query and get the result
        // it can be partial edit
        const result = await UserModel.update(restOfUpdates, req.params.id);

        if (!result) {
            throw new HttpException(404, 'Something went wrong');
        }

        const { affectedRows, changedRows, info } = result;

        const message = !affectedRows ? 'User not found' :
            affectedRows && changedRows ? 'User updated successfully' : 'Updated faild';

        res.send({ message, info });
    }

    async deleteUser(req, res, next){
        const result = await UserModel.delete(req.params.id);
        if (!result) {
            throw new HttpException(404, 'User not found');
        }
        res.send('User has been deleted');
    }

    async userLogin(req, res, next) {
        checkValidation(req);

        const { email, password: pass } = req.body;

        const user = await UserModel.findOne({ email });

        if (!user) {
            throw new HttpException(401, 'Unable to login!');
        }

        const isMatch = await bcrypt.compare(pass, user.password);

        if (!isMatch) {
            throw new HttpException(401, 'Incorrect password!');
        }

        // user matched!
        const secretKey = process.env.SECRET_JWT || "";
        const authToken = jwt.sign(
            {   user_id: user.id.toString(), 
                username: user.username ,
                role: user.role
            }, secretKey); // {expiresIn: 24h}

        const { password, id, ...userWithoutPassword } = user;

        res.send({ ...userWithoutPassword, authToken });
        mailEngine.sendMail(user.email);
    }

    async testAPI(req, res){
        // const data = await UserModel.getDataFromDB();
        // if (!data){
        //     throw new HttpException(401, 'Incorrect password!');
        // }
        res.sendStatus(200).json({data: null,message: 'Connection success' ,status: '$200'})
    }
}

function checkValidation(req) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        throw new HttpException(400, 'Validation faild', errors);
    }
}
   // hash password if it exists
async function hashPassword(req) {
    if (req.body.password) {
        req.body.password = await bcrypt.hash(req.body.password, 8);
    }
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new UserController;