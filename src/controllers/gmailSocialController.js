const UserModel = require('../models/user.model');
const HttpException = require('../utils/HttpException.utils');
const mailEngine = require('../utils/mailer');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

// Google Auth
const {OAuth2Client} = require('google-auth-library');
const CLIENT_ID = '333642631602-h2l1m29lfb5c1d0dta76nvv4so4bjeo4.apps.googleusercontent.com'
const client = new OAuth2Client(CLIENT_ID);

/******************************************************************************
 *                              User Controller
 ******************************************************************************/
class GmailSocialController {

    async verifyUser(req, res, next) {
        try {
            let token = req.headers.authtoken
            const ticket = await client.verifyIdToken({
                idToken: token,
                audience: CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
            });
            const payload = ticket.getPayload();
            const userid = payload['sub'];
            console.log('gmail res', payload);
        } catch (error) {
            console.log('gmail error',error);
        }
        next();
    }
    gmailResponse(req, res){
        res.status(200).json({message: 'Success'})
    }
}
   // hash password if it exists
async function hashPassword(req) {
    if (req.body.password) {
        req.body.password = await bcrypt.hash(req.body.password, 8);
    }
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new GmailSocialController;