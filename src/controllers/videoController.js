const http = require('http');
const express = require('express');
const socketio = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketio(server, {
    cors: {
        origin: "*"
      }
});

const PORT = 8087;

class videoController{
    constructor(){
        // this.handleSocketConnection();
    }
    initVideo(req, res){
        res.status(200).json({
            status: '$200', 
            message: 'Connection success',
            data: null
        })
    }
    handleSocketConnection() {
        console.log('called socket');
        io.on('connection', (socket) => {
            console.log('Connection established!!!',socket.handshake);
            setInterval(() => {
                socket.emit('test event', 'Got data from websoket server'); 
            }, 3000);
        });
        server.listen(PORT, () => console.log(`Server running on port ${PORT}`));
    }
}

module.exports = new videoController;