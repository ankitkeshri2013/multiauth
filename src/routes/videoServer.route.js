const express = require('express');
const route = express.Router();
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');
const videoController = require('../controllers/videoController');

route.get('/joinVideo', awaitHandlerFactory(videoController.handleSocketConnection)); //api/video/joinVideo

module.exports = route;