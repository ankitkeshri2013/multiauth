const express = require('express');
const imageController = require('../controllers/imageUpload.controller');
const imageUploader = require('../utils/imageUpload.utils');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');
const auth = require('../middleware/auth.middleware');
const Role = require('../utils/userRoles.utils');
const router = express.Router();

router.post('/upload',auth(Role.SuperUser), imageController.upload);

module.exports = router;