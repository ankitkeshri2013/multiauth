const express = require('express');
const gmailSocialController = require('../controllers/gmailSocialController');
const router = express.Router();
const { verifyGmailToken, checkIfUserAvailale } = require('../utils/common.utils');
const UserModel = require('../models/user.model');

router.post('/loginWithGmail',(req, res) => {
    let token = req.headers.authtoken;
    verifyGmailToken(token).then( async (userData) => {
        console.log('gmail login success, checking user in DB...');
        const user = await UserModel.findOne({ email: userData['email'] });
        if (user) {
            console.log('user already in DB, sending success responce');
            res.send({status: 'success', userData: userData});
        } else {
            console.log('Could not found user in DB, creating user...');
            let payload = {
                user_id: userData['email'],
                name: userData['name'],
                email: userData['email'],
                gender: 'Male',
                mobile: '9036467468',
                profile_pic_url: userData['picture'],
                created_at: new Date().getTime(),
                updated_at: new Date().getTime(),
            }
            console.log('payload', payload);
            const result = await UserModel.createUser(payload);
            if (!result) {
                throw new HttpException(500, 'Something went wrong');
            }
            res.status(201).json({status: 'success', userData: data});
        }
      }).catch((error) => {
          console.log('gmail error', error);
          res.send({status: 'failed'})
      });
    // console.log('gmailRes', gmailRes);
    // res.send(gmailRes);
    // return;
    // async function verify() {
    //     const ticket = await client.verifyIdToken({
    //         idToken: token,
    //         audience: CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
    //     });
    //     payload = ticket.getPayload();
    //     const userid = payload['sub'];
    //     return payload;
    //   }
    //   verify().then((data)=>{
    //     console.log('gmail login success', data);  
    //       res.send({status: 'success', userData: data});
    //   }).catch((error) => {
    //       console.log('gmail error', error);
    //       res.send({status: 'failed'})
    //   });
});

module.exports = router;