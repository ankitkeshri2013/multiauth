const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
// Google Auth
const {OAuth2Client} = require('google-auth-library');
const CLIENT_ID = '722543070610-1bnmcjh5bp3qai4vqe4l9cqsk4f55lkr.apps.googleusercontent.com';
const client = new OAuth2Client(CLIENT_ID);
const UserModel = require('../models/user.model');

dotenv.config();

exports.getPlaceholderStringForArray = (arr) => {
    if (!Array.isArray(arr)) {
        throw new Error('Invalid input');
    }

    // if is array, we'll clone the arr 
    // and fill the new array with placeholders
    const placeholders = [...arr];
    return placeholders.fill('?').join(', ').trim(); 
}


exports.multipleColumnSet = (object) => {
    if (typeof object !== 'object') {
        throw new Error('Invalid input');
    }

    const keys = Object.keys(object);
    const values = Object.values(object);

    let columnSet = keys.map(key => `${key} = ?`).join(', ');

    return {
        columnSet,
        values
    }
}

exports.getResponceObject = (type, status, message) => {
    return {
        type,
        status,
        message
    }
}

exports.getCurrentUserByToken = (token) => {
    try {
        if(!token) { return false}
        return jwt.verify(token, process.env.SECRET_JWT);
    } catch (error) {
        console.log(error);
    }
}

exports.verifyGmailToken = async  (token) => {

    console.log('verify for token');
    let payload = null;
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
    });
    payload = ticket.getPayload();
    return payload;  
}

exports.checkIfUserAvailale = async (userData) => {
    console.log('UserModel', UserModel);
    const user = await UserModel.findOne({ username: userData['email'] });
    if (user) {
        res.send({status: 'success', userData: data});
    } else {
        let payload = {
            user_id: userData['email'],
            first_name: userData['firstName'],
            last_name: userData['lastName'],
            email: userData['email'],
            gender: 'Male',
            mobile: '9036467468',
            profile_pic: userData['photoUrl'],
            created_at: new Date().getTime(),
            updated_at: new Date().getTime(),
        }
        const result = await UserModel.create(payload);
        if (!result) {
            throw new HttpException(500, 'Something went wrong');
        }
        res.status(201).json({status: 'success', userData: data});
    }
}
