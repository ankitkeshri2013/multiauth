const dotenv = require("dotenv");
dotenv.config();
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.GMAIL_USER,
    pass: process.env.GMAIL_PASS,
  },
});

class MailEngine {
  sendMail(to, subject='', body='') {
    if (process.env.STOP_MAIL_SERVICE_GLOBAL == false) {
      const mailOptions = {
        from: "Node server admin",
        to: to,
        subject: subject || "Sending Email using Node.js",
        text: body || "You are logged in.",
      };

      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log("Email sent: " + info.response);
        }
      });
    }
  }
}

module.exports = new MailEngine();
