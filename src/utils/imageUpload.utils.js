// const multer = require('multer');
// const path = require('path');
// const HttpException = require('../utils/HttpException.utils');
// const storage = multer.diskStorage({
//     destination: function(req, file, cb){
//         cb(null, './uploads');
//     },
//     filename: function(req, file, cb){
//         cb(null, new Date().getTime() + path.extname(file.originalname));
//     }
// });

// const fileFilter = (req, file, cb) => {
//     if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
//         cb(null, true);
//     }else{
//         cb(new Error('Unsupported file type'), false);
//         // throw new HttpException(401, 'File type not supported.')
//     }
// }

// const upload = multer({
//     storage: storage,
//     limits: {
//         fileSize:1024
//     },
//     fileFilter:fileFilter
// });

// module.exports = {
//     upload: upload
// }

const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/) 
        && (file.mimetype != 'image/jpeg' || file.mimetype != 'image/png')) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};
exports.imageFilter = imageFilter;