const query = require('../db/db-connection');
const { multipleColumnSet } = require('../utils/common.utils');
const Role = require('../utils/userRoles.utils');
const tableName = 'userData';
class UserModel {
    

    async find(params = {}) {
        let sql = `SELECT * FROM ${tableName}`;

        if (!Object.keys(params).length) {
            return await query(sql);
        }

        const { columnSet, values } = multipleColumnSet(params);
        sql += ` WHERE ${columnSet}`;

        return await query(sql, [...values]);
    }

    async findOne(params) {
        const { columnSet, values } = multipleColumnSet(params);

        const sql = `SELECT * FROM ${tableName}
        WHERE ${columnSet}`;
        const result = await query(sql, [...values]);

        // return back the first row (user)
        return result[0];
    }

    async getDataFromDB(){

        const sql = `SELECT * FROM users_1`;

        const result = await query(sql);

        // return back the first row (user)
        return result;
    }

    async create({ username, password, first_name, last_name, email, role = Role.SuperUser, age = 0 }){
        const sql = `INSERT INTO ${tableName}
        (username, password, first_name, last_name, email, role, age) VALUES (?,?,?,?,?,?,?)`;

        const result = await query(sql, [username, password, first_name, last_name, email, role, age]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }

    async createUser({ user_id, name, email, gender, mobile, profile_pic_url, created_at, updated_at }){
        const sql = `INSERT INTO ${tableName}
        (user_id, name, email, gender, mobile, profile_pic_url, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?)`;
        const result = await query(sql, [user_id, name, email, gender, mobile, profile_pic_url, created_at, updated_at]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }

    async update(params, id) {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `UPDATE user SET ${columnSet} WHERE id = ?`;

        const result = await query(sql, [...values, id]);

        return result;
    }

    async delete(id) {
        const sql = `DELETE FROM ${tableName}
        WHERE id = ?`;
        const result = await query(sql, [id]);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }
    async updateProfilePicUrl(username, params){
        const { columnSet, values } = multipleColumnSet(params);
        const sql = `UPDATE ${tableName} SET profilePicURL='${params.profilePicUrl}' WHERE username = '${username}'`;
        console.log('update qurery', sql);
        const result = await query(sql);
        return result;
    }
}

module.exports = new UserModel;