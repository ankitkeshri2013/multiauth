const express = require("express");
const dotenv = require('dotenv');
const cors = require("cors");
const HttpException = require('./utils/HttpException.utils');
const errorMiddleware = require('./middleware/error.middleware');
const userRouter = require('./routes/user.route');
const imageRoute = require('./routes/imageUpload.route');
const webSocketRoute = require('./routes/videoServer.route');
const GmailSocialController = require('./routes/gmail-social-login');

// Init express
const app = express();
// Init environment
dotenv.config();
// parse requests of content-type: application/json
// parses incoming requests with JSON payloads
app.use(express.json());
// enabling cors for all requests by using cors middleware
app.use(cors());
// Enable pre-flight
app.options("*", cors());

const port = Number(process.env.PORT || 3331);

app.use(`/api/v1/users`, userRouter);
// Image Upload and read route
app.use("/api/v1/image", imageRoute);
app.use('/api/v1/image/uploads', express.static('uploads'));
//Social login with Gmail
app.use('/api/v1/social', GmailSocialController);
//Live websocket APIs
app.use('/api/video', webSocketRoute);
// 404 error
app.all('*', (req, res, next) => {
    const err = new HttpException(404, 'Endpoint Not Found');
    next(err);
});

// Error middleware
app.use(errorMiddleware);

// starting the server
app.listen(port, () =>
    console.log(`🚀 Server re-launch on port ${port}!`));


module.exports = app;